# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 13:57:44 2017

@author: Elie

Class that implements Filtered Backprojection (FBP) algorithms for sinograms obtained from pencil and cone beams
"""

from image import Image
import numpy as np
from scipy.fftpack import fft, ifft, fftfreq, fft2, ifft2

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)

# FBP class
class FBP():
    """
    A class that implements Filtered Backprojection (FBP) algorithms for sinograms obtained from pencil and cone beams
    """
    
    def compute(sinogram, thetas, filtering=True, detail=False):
        """
        Compute the reconstruction using FBP for a pencil beam
        
        Given the list of the thetas for which the attenuation was computed to obtain the sinogram, computes a reconstructed image using Inverse Radon Transform (IRTF), with or without filtering (FBP or BP).
        
        Parameters
        ----------
        sinogram : Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
                
        filtering : boolean
            defines if FBP or BP
            
        detail : boolean
            keep track of the reconstruction steps or not
            
        
        Returns
        -------
        fbp : depending on detail value, numpy 2-D array or numpy 3-D array
            depending on detail value, just the final reconstruction image (False) or an array of images containing each step of the reconstruction (True) (one step = one theta)
        """
        
        size = sinogram.img_size
        sng_size = sinogram.size
        
        # Initialize fbp image or fbp array of images with the same size than that of the phantom
        fbp = np.zeros((size, size)) if not detail else np.zeros((len(thetas), size, size))
        
        # Filtering is made in Fourier space, spatial frequencies being distances to the center of the image
        if filtering:
            # Get the fft frequencies (distances to the center) and the ramp filter associated to the IRTF
            f = fftfreq(sng_size).reshape(-1, 1)
            ramp_filter = 2 * np.abs(f)

            # Correct sinogram with ramp filter in Fourier space
            f_proj = fft(sinogram.sng, axis=0) * ramp_filter
            f_sinogram = np.real(ifft(f_proj, axis=0))
        else:
            f_sinogram = np.copy(sinogram.sng)
            
        # X, Y coordinates of the pixels of the image on which we will interpolate
        [Y, X] = np.mgrid[0:size, 0:size] - size // 2

        # Reconstruct image by interpolation slice by slice of the sinogram (theta by theta)
        th = np.deg2rad(thetas)
        for i in range(len(th)):
            # IRTF is computed by integrating the sinogram on space at points x*cos(theta) + y*sin(theta), t contains these points for a given theta
            t = X * np.cos(th[i]) + Y * np.sin(th[i])
            
            # We interpolate the data defined by x abscissas and the sinogram slice given by theta at all the points in t
            rho = np.arange(sng_size) - sng_size//2
            backproj = np.interp(t, rho, f_sinogram[:, i],
                                 left=0, right=0)
            
            # We add the result to the fbp reconstructed image for the new theta considered (keeping track of previous fbps if wanted)
            if detail:
                if i != 0:
                    fbp[i] += fbp[i-1]
                fbp[i] += backproj * np.pi / (2 * len(th))
            else:
                fbp += backproj * np.pi / (2 * len(th))

        return fbp
    
    def compute_bpf(sinogram, thetas):
        """
        Compute the reconstruction using BPF for a pencil beam
        
        Given the list of the thetas for which the attenuation was computed to obtain the sinogram, computes a filtered reconstructed image using Inverse Radon Transform (IRTF).
        
        Parameters
        ----------
        sinogram : Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
            
        
        Returns
        -------
        bpf : numpy 2-D array
        """
        
        size = sinogram.img_size
        sng_size = sinogram.size
        
        # Initialize fbp image or fbp array of images with the same size than that of the phantom
        bpf = np.zeros((size, size))
            
        # X, Y coordinates of the pixels of the image on which we will interpolate
        [Y, X] = np.mgrid[0:size, 0:size] - size // 2

        # Reconstruct image by interpolation slice by slice of the sinogram (theta by theta)
        th = np.deg2rad(thetas)
        for i in range(len(th)):
            # IRTF is computed by integrating the sinogram on space at points x*cos(theta) + y*sin(theta), t contains these points for a given theta
            t = X * np.cos(th[i]) + Y * np.sin(th[i])
            
            # We interpolate the data defined by x abscissas and the sinogram slice given by theta at all the points in t
            rho = np.arange(sng_size) - sng_size//2
            backproj = np.interp(t, rho, sinogram.sng[:, i],
                                 left=0, right=0)
            
            # We add the result to the fbp reconstructed image for the new theta considered
            bpf += backproj * np.pi / (2 * len(th))
        
        # Filtering is made in Fourier space, spatial frequencies being distances to the center of the image
        # Get the fft frequencies (distances to the center) and the ramp filter associated to the IRTF
        f = fftfreq(size).reshape(-1, 1)
        ramp_filter = 2 * np.abs(f)

        # Correct sinogram with ramp filter in Fourier space
        f_proj = fft2(bpf) * np.sqrt(np.outer(ramp_filter, ramp_filter))
        bpf = np.real(ifft2(f_proj))

        return bpf
    
    
    def compute_fan(sinogram, thetas, src_dist, filtering=True, detail=False):
        """
        Compute the reconstruction using FBP for a fan beam
        
        Given the list of the thetas for which the attenuation was computed to obtain the sinogram, computes a reconstructed image using Inverse Radon Transform (IRTF), with or without filtering (FBP or BP), knowing the ray source distance from the center of the image.
        
        Parameters
        ----------
        sinogram : Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
        
        src_dist : scalar
            distance in pixels of the source to the center of the image, supposing a circular trajectory of the source
                        
        filtering : boolean
            defines if FBP or BP
            
        detail : boolean
            keep track of the reconstruction steps or not
            
        
        Returns
        -------
        fbp : depending on detail value, numpy 2-D array or numpy 3-D array
            depending on detail value, just the final reconstruction image (False) or an array of images containing each step of the reconstruction (True) (one step = one theta)
        """
        
        size = sinogram.size
        img_size = sinogram.img_size
        sng_size = sinogram.sng_size
        
        # Initialize fbp image or fbp array of images
        fbp = np.zeros((img_size, img_size)) if not detail else np.zeros((len(thetas), img_size, img_size))
        
        # Each sinogram row corresponds to a ray angle alpha and is modulated by D * cos(alpha)
        sng = np.copy(sinogram.sng)
        alphas = np.arctan((np.arange(sng_size) - sng_size // 2) / (src_dist + size // 2))
        for i in range(len(thetas)):
            sng[:, i] = (src_dist + size // 2) * sng[:, i] * np.cos(alphas)
            
        # Filtering is made in Fourier space, frequencies being in fact distances to the center of the image
        if filtering:
            # Get the fft frequencies (distances to the center) and the ramp filter associated to the IRTF
            f = fftfreq(sng_size).reshape(-1, 1)
            ramp_filter = 2 * np.abs(f)

            # Correct sinogram with ramp filter in Fourier space
            f_proj = fft(sng, axis=0) * ramp_filter
            f_sinogram = np.real(ifft(f_proj, axis=0))
        else:
            f_sinogram = np.copy(sinogram.sng)
            
        # After filtering, each sinogram row is modulated by (alpha / sin(alpha)**2 / 2
        for i in range(sng_size):
            alpha = np.arctan((i - sng_size // 2) / (src_dist + size // 2))
            f_sinogram[i, :] *= (alpha / np.sin(alpha))**2 / 2. if np.sin(alpha) != 0. else 0.5
                        
        [Y, X] = np.mgrid[0:img_size, 0:img_size] - img_size // 2
        rho, phi = np.zeros((img_size, img_size)), np.zeros((img_size, img_size))
        for i in range(img_size):
            for j in range(img_size):
                rho[i, j], phi[i, j] = cart2pol(X[i, j], Y[i, j])
        
        th = np.deg2rad(thetas)
        
        # Source coordinates relative to the center of the image
        src_x = src_dist * np.cos(th)
        src_y = src_dist * np.sin(th)
        
        inv_squared_dists = np.zeros((img_size, img_size))
        alpha_primes = np.zeros((img_size, img_size))
            
        # Reconstruct image by interpolation slice by slice of the sinogram (theta by theta)
        for k in range(len(th)):
            # IRTF is computed by integrating the sinogram on space at points x*cos(theta) + y*sin(theta), t contains these points for a given theta
            R_squared = (X - src_x[k])**2 + (Y - src_y[k])**2
            R = np.sqrt(R_squared)
            inv_squared_dists = 1. / R_squared
            alpha_primes = np.arcsin(np.cos(th[k] - phi) * rho / R)
           
            alpha_primes[np.isnan(alpha_primes)] = 0.
            
            # We interpolate the data defined by x abscissas and the sinogram slice given by theta at all the points in t
            backproj = np.interp(alpha_primes, alphas, f_sinogram[:, k],
                                 left=0, right=0)
            backproj = backproj * inv_squared_dists
            
            # We add the result to the fbp reconstructed image for the new theta considered (keeping track of previous fbps if wanted)
            if detail:
                if i != 0:
                    fbp[i] += fbp[i-1]
                fbp[i] += backproj * np.pi / (2 * len(th))
            else:
                fbp += backproj * np.pi / (2 * len(th))

        return fbp
    
    
    def compute_cone_chord(sinogram, thetas):
        """
        Compute the reconstruction on a chord using FBP for a cone beam [zuo2006]
        
        Given the list of the thetas for which the attenuation was computed to obtain the sinogram, computes an image using the FBP reconstruction algorithm on a chord for cone beam given in [zuo2006].
        
        Parameters
        ----------
        sinogram : Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
                
        
        Returns
        -------
        fbp : numpy 2-D array
        """
        
        # Compute the sinogram derivative according to the angle theta (related to curvilign abscissa)
        d = np.gradient(sinogram.sng, axis=0)
        size = sinogram.img_size
        
        # Chord extremities from angles (first and last of thetas) and their coordinates in space (on the circular trajectory of the cone beam source)
        tar = np.deg2rad(thetas[0])
        c, s = np.cos(tar), np.sin(tar)
        sa_x = (size // 2) * (1 + c)
        sa_y = (size // 2) * (1 + s)
        
        tbr = np.deg2rad(thetas[len(thetas) - 1])
        c, s = np.cos(tbr), np.sin(tbr)
        sb_x = (size // 2) * (1 + c)
        sb_y = (size // 2) * (1 + s)
        
        chord_size = np.sqrt((sb_x - sa_x)**2 + (sb_y - sa_y)**2)
        
        # Rounded chord points coordinates
        taus = np.linspace(0, 1, chord_size)
        i_pt_x = np.array([sa_x + (sb_x - sa_x) * tau for tau in taus])
        i_pt_y = np.array([sa_y + (sb_y - sa_y) * tau for tau in taus])
        
        pt_x = [i_pt_x[0]]
        pt_y = [i_pt_y[0]]
        for k in range(1, len(i_pt_x)):
            if round(i_pt_x[k], 0) != round(i_pt_x[k - 1], 0) or round(i_pt_y[k], 0) != round(i_pt_y[k - 1], 0):
                pt_x.append(round(i_pt_x[k], 0))
                pt_y.append(round(i_pt_y[k], 0))
        pt_x = np.int16(pt_x)
        pt_y = np.int16(pt_y)
        
        th = np.deg2rad(thetas)
        c, s = np.cos(th), np.sin(th)
        
        # Source coordinates
        src_x = (size // 2) * (1. + c)
        src_y = (size // 2) * (1. + s)
        
        # Receptor center coordinates
        rcp_center_x = (size // 2) * (1. - c)
        rcp_center_y = (size // 2) * (1. - s)
        
        # Ray vector
        ray_vec_x = - (rcp_center_x - src_x) / size
        ray_vec_y = - (rcp_center_y - src_y) / size
        
        # Vector defining the receptor orientation
        rcp_vec_x, rcp_vec_y = np.copy(s), - np.copy(c)
        
        # For each point on the chord
        backproj = np.zeros((size, size))
        for i in range(len(pt_x)):
            if not sinogram.in_img(pt_x[i], pt_y[i]):
                continue
            
            # For each theta (related to the curvilign abscissa)
            bp = 0.
            for k in range(len(th)):
                # Calculate first integral
                a = size / ((pt_x[i] - src_x[k]) * ray_vec_x[k] + (pt_y[i] - src_y[k]) * ray_vec_y[k])

                # Calculate uc and upcs (cone-beam projections on the receptor for the considered point of the chord and for all the points on the chord)
                w1 = ((src_x[k] - sa_x) * ray_vec_x[k] + (src_y[k] - sa_y) * ray_vec_y[k])
                w2 = ((src_x[k] - sb_x) * ray_vec_x[k] + (src_y[k] - sb_y) * ray_vec_y[k])
                
                upcs = np.array([w2 * tau * chord_size / (w1 * (1 - tau) * chord_size + w2 * tau * chord_size) for tau in taus])

                uc = w2 * taus[i] * chord_size / (w1 * (1 - taus[i]) * chord_size + w2 * taus[i] * chord_size)
                
                # The cone-beam projection for which we need the value of the derivative of the sinogram (by interpolation)
                uf = size * ((pt_x[i] - src_x[k]) * rcp_vec_x[k] + (pt_y[i] - src_y[k]) * rcp_vec_y[k]) / ((pt_x[i] - src_x[k]) * ray_vec_x[k] + (pt_y[i] - src_y[k]) * ray_vec_y[k])

                # Calculate second integral
                b = 0.
                x = np.arange(sinogram.size) - sinogram.size//2
                for j in range(len(upcs)):
                    b += np.interp(uf, x, d[:, k], left=0, right=0) / (uc - upcs[j])
                b /= np.sqrt((pt_x[i] - src_x[k])**2 + (pt_y[i] - src_y[k])**2)
                
                # bp is the backprojection for the considered chord point
                bp += a * b
            
            backproj[pt_x[i], pt_y[i]] = bp
        
        return backproj
    
    
    def compute_cone(sinogram, thetas):
        """
        Compute the reconstruction using FBP for a cone beam [zuo2006]
        
        Given the list of the thetas for which the attenuation was computed to obtain the sinogram, computes an image using the FBP reconstruction algorithm for cone beam given in [zuo2006].
        
        Parameters
        ----------
        sinogram : Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
                
        
        Returns
        -------
        fbp : numpy 2-D array
        """
        # Initialize fbp image
        fbp = np.zeros((sinogram.img_size, sinogram.img_size))
        
        # For each chord such that theta[first] <= theta[i] < theta[j] <= theta[last]
        # For M chords, it computes M*(M-1)/2 chords
        for i in range(len(thetas) - 1):
            for j in range(i + 1, len(thetas)):
                fbp += FBP.compute_cone_chord(sinogram, thetas[i:j + 1])