# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 13:56:19 2017

@author: Elie

Class to compute sinograms from phantoms
"""

import numpy as np
from scipy.misc import imrotate, imread
from scipy.interpolate import interp2d, RectBivariateSpline

class Sinogram(object):
    """
    A class to compute a sinogram from a phantom
    """
    # Constructor:
    #   - A phantom
    # It creates the padded image and initializes an empty sinogram
    def __init__(self, phantom):
        """
        Constructor
        
        Given a phantom object, initializes a sinogram object
        
        Parameters
        ----------
        phantom : Phantom
            object from which the sinogram will be computed
        """
        
        self.phantom = phantom
        
        # The padded image p_img has the size of the diagonal of the former image, it is better to reduce border effects
        # Keep the padded image size as field
        self.img_size = phantom.img.shape[0]
        self.size = int(self.img_size * np.sqrt(2))
        self.sng_size = self.size
        self.p_img = np.zeros((self.size, self.size))
        self.pad = (self.size - self.img_size)//2
        for i in range(self.img_size):
            for j in range(self.img_size):
                self.p_img[i + self.pad, j + self.pad] = phantom.img[i, j]
        
        # Initiate a sng field that will contain the sinogram image at None
        self.sng = None
    
    
    def from_file(self, filepath):
        """
        Import the sinogram from an image file given its filepath
        
        Parameters
        ----------
        filepath : string
            path of the file
        """
        self.sng = np.float64(imread(filepath))
        self.sng_size = self.sng.shape[0]
        
   
    def compute(self, thetas):
        """
        Compute the sinogram for a pencil beam
        
        Given a list of thetas for which attenuation is computed using the Radon Transform, computes the sinogram obtained with a pencil beam.
        
        Parameters
        ----------
        thetas : numpy 1-D array
            angles for which we compute the attenuation
        """
        
        # Abscissas of the sinogram are the projection angles
        # Ordinates are the size of the image (the pencil beam moves from top to the bottom of the image, its ray coming out in the transverse direction)
        self.sng = np.zeros((self.sng_size, len(thetas)))

        # Instead of computing the rays equations from thetas, we rotate the image for each theta
        # The pencil beam movement remains the same from top to bottom so the attenuation corresponds to the integral (sum of the pixels) on each horizontal line of the image
        for k in range(len(thetas)):
            r_img = imrotate(self.p_img, thetas[k])
            self.sng[:, k] = r_img.sum(0)
            
            
    def compute_fan(self, thetas, src_dist, alpha, interp=False):
        """
        Compute the sinogram for a fan beam
        
        Given a list of thetas for which attenuation is computed using the Radon Transform, computes the sinogram obtained with a fan beam of angle alpha, using a precise but slower interpolation between pixels of the image or without interpolation.
        
        Parameters
        ----------
        thetas : numpy 1-D array
            angles for which we compute the attenuation
        
        alpha : scalar
            fan beam angle
        
        interp : boolean
            defines if interpolation between pixels is used or not
        
        NOTE : interpolation is very slow, and may not be really useful for what we expect
        """
        
        # Abscissas of the sinogram are the projection angles
        # Ordinates are the size of the receptor (diameter of the fan beam at the receptor level x=img_size)
        alpha = np.deg2rad(alpha)
        self.sng_size = int(2 * (src_dist + self.size // 2) * np.tan(alpha))
        self.sng = np.zeros((self.sng_size, len(thetas)))
        
        # Compute the relative ordinates of the fan beam rays at the receptor level
        beginning_size = (src_dist - self.size / 2.) * np.tan(alpha)
        begin_ys = np.linspace(0., beginning_size, self.sng_size) - beginning_size / 2.
        final_ys = np.arange(self.sng_size) - self.sng_size / 2.
        
        # xs contains abscissas of points along the rays from 0 to receptor ordinate = img_size
        # ys contains ordinates of points along the rays : there is a ray for each ordinate in final_ys
        xs = np.arange(self.size)
        ys = np.zeros((self.sng_size, self.size))
        for i in range(self.sng_size):
            ys[i] = self.size // 2 + np.linspace(begin_ys[i], final_ys[i], self.size)
        
        # Instead of computing the rays equations from thetas, we rotate the image for each theta
        # The rays points remain the same from image to image, it is the image pixel values that change
        for k in range(len(thetas)):
            r_img = imrotate(self.p_img, thetas[k])
            
            # Compute the interpolation function if interp=True
            if interp:
                arr = np.arange(0, r_img.shape[0])
                X, Y = np.meshgrid(arr, arr)
                f = interp2d(X, Y, r_img, fill_value=0)
            
            # For each ray, compute the array of pixel values along the ray to integrate (sum) along the ray, by interpolating or taking closest pixels to point coordinates
            for i in range(self.sng_size):
                vals = f(ys[i], xs) if interp else np.array([r_img[xs[j], int(ys[i, j])] if self.in_img(xs[j], int(ys[i, j])) else 0 for j in range(self.img_size)])
                self.sng[i, k] = np.sum(vals)
    
    
    def compute_fan_non_opt(self, thetas, src_dist, alpha, interp=False):
        """
        Compute the sinogram for a fan beam (non optimal)
        
        Given a list of thetas for which attenuation is computed using the Radon Transform, computes the sinogram obtained with a fan beam of angle alpha, using a precise but slower interpolation between pixels of the image or without interpolation.
        This version is non optimal because written before understanding that interp2d is not slow to call, but the function f it results in is slow to call. This version was made in order to limit calls to interp2d (1 call instead of len(thetas) calls).
        
        Parameters
        ----------
        thetas : numpy 1-D array
            angles for which we compute the attenuation
        
        alpha : scalar
            fan beam angle
        
        interp : boolean
            defines if interpolation between pixels is used or not
        
        NOTE : interpolation is very slow, and may not be really useful for what we expect
        """
        
        # Abscissas of the sinogram are the projection angles
        # Ordinates are the size of the receptor (diameter of the fan beam at the receptor level)
        alpha = np.deg2rad(alpha)
        self.sng_size = int(2 * (src_dist + self.size // 2) * np.tan(alpha))
        self.sng = np.zeros((self.size, len(thetas)))
        
        angles = np.linspace(- alpha, alpha, self.size)
        
        if interp:
            arr = np.arange(0, self.img_size)
            X, Y = np.meshgrid(arr, arr)
            f = interp2d(X, Y, self.p_img, fill_value=0)

        # Cosine and sine of each rotation angle
        th = np.deg2rad(thetas)
        c, s = np.cos(th), np.sin(th)
        
        # Source coordinates after rotation
        src_x, src_y = src_dist * (1. + c), src_dist * (1. + s)
        
        # Receptor center coordinates
        rcp_center_x, rcp_center_y = (self.img_size // 2) * (1. - c), (self.img_size // 2) * (1. - s)
        
        # Coordinates of the vector defining the orientation of the flat-panel receptor
        rcp_vec_x, rcp_vec_y = np.copy(s), - np.copy(c)
        
        for k in range(len(th)):
            # Points of the receptor for which we will compute the attenuation on a ray coming from the source to these points
            rcp_xs = np.array([rcp_center_x[k] + rcp_vec_x[k] * np.tan(a) * self.img_size for a in angles])
            rcp_ys = np.array([rcp_center_y[k] + rcp_vec_y[k] * np.tan(a) * self.img_size for a in angles])
            
            # Compute sinogram slice ray by ray
            for i in range(len(rcp_xs)):
                # Ray points coordinates (from source to receptor)
                line_xs = np.linspace(src_x[k], rcp_xs[i], self.img_size)
                line_ys = np.linspace(src_y[k], rcp_ys[i], self.img_size)
          
                vals = f(line_ys, line_xs) if interp else np.array([self.p_img[int(line_xs[j]), int(line_ys[j])] if self.in_img(line_xs[j], line_ys[j]) else 0 for j in range(len(line_xs))])
                self.sng[i, k] = np.sum(vals)
                
                ## Draw fan beam envelope
                #if i == 0 or i == self.img_size - 1:
                #    for j in range(len(line_xs)):
                #        if self.in_img(line_xs[j], line_ys[j]):
                #            self.p_img[int(line_xs[j]), int(line_ys[j])] = 255
    
    
    def in_img(self, x, y):
        """
        Check if a point is inside sinogram phantom or not from its coordinates x and y
        """
        if x < 0 or y < 0 or x >= self.img_size or y >= self.img_size:
            return False
        return True