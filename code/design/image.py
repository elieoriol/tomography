# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 15:12:16 2017

@author: Elie

Class to plot images and reduce noise on an image
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft2, fftshift, ifft2

# Phantom class
#   - The phantom image
#   - A padded image (supplementary borders)
#   - The associated sinogram (to compute)
class Image():
    """
    A class to plot images and reduce noise on an image
    """

    def plot(img, axis, **options):
        """
        Plot an image. 
        
        Given a numpy 2-D array, a figure axis and some options, plots the image contained in the array.
        
        Parameters
        ----------
        img : numpy 2-D array
            img[i, j] contains the pixel value at position (i, j)
        
        axis : matplotlib.axes.Axes object
        
        options : list of options separated by commas
            x_label : sets the x label of the plot
            y_label : sets the y label of the plot
            title : sets the title of the plot
            extent : sets the extent (xmin, xmax, ymin, ymax) of the plot
            aspect : sets the aspect of the plot
            vmin, vmax : to normalize luminance data
        """
        # Set options
        if 'x_label' in options:
            axis.set_xlabel(options['x_label'], fontsize=16)
        
        if 'y_label' in options:
            axis.set_ylabel(options['y_label'], fontsize=16)
        
        if 'title' in options:
            axis.set_title(options['title'], fontsize=20)
            
        ext = options['extent'] if 'extent' in options else None
        asp = options['aspect'] if 'aspect' in options else None
        vmin = options['vmin'] if 'vmin' in options else None
        vmax = options['vmax'] if 'vmax' in options else None

        # Plot
        axis.imshow(img, cmap=plt.cm.Greys_r, extent=ext, aspect=asp, vmin=vmin, vmax=vmax)
    
    
    def plot_fft(img, axis, **options):
        """
        Plot fft of an image.
        
        Given a numpy 2-D array, a figure axis and some options, plots the fft of the image contained in the array.
        
        Parameters
        ----------
        img : numpy 2-D array
            img[i, j] contains the pixel value at position (i, j)
        
        axis : matplotlib.axes.Axes object
        
        options : list of options separated by commas
            title : sets the title of the plot
        """
        
        # Compute the centered absolute 2-D fft of the image
        fft = fft2(img)
        abs_fft = abs(fft)
        centered_abs_fft = fftshift(abs_fft)
        
        # Set options
        if 'title' in options:
            axis.set_title(options['title'], fontsize=22)
            
        # Plot
        axis.imshow(centered_abs_fft, cmap=plt.cm.Greys_r)
    
    
    def reduce_noise(img, n_f=0.):
        """
        Reduce gaussian noise on an image.
        
        Given a numpy 2-D array and a narrow factor, reduces the gaussian noise on an image using an inversed hanning window on image spatial frequencies.
        
        Parameters
        ----------
        img : numpy 2-D array
            img[i, j] contains the pixel value at position (i, j)
        
        n_f : scalar
            narrow factor to narrow the hanning window
            
        Returns
        -------
        The filtered image : numpy 2-D array
        """
        
        # Get the 2-D fft of the image
        fft = fft2(img)
        
        # Initialize arrays that will contain the inversed hanning windows on both x and y axes
        win0 = np.zeros(fft.shape[0])
        win1 = np.zeros(fft.shape[1])
        
        # Set the limits in which to narrow the inversed hanning windows using the narrow factor
        ll0, ll1 = int(n_f * fft.shape[0]), int(n_f * fft.shape[1])
        ul0, ul1 = fft.shape[0] - ll0, fft.shape[1] - ll1
        
        # Compute inversed hanning windows in narrowed limits
        win0[ll0:ul0] = 1 - np.hanning(fft.shape[0] - 2 * ll0)
        win1[ll1:ul1] = 1 - np.hanning(fft.shape[1] - 2 * ll1)
        
        # Product of both windows to obtain a 2-D narrowed inversed hanning window, apply it to 2-D fft
        win = np.sqrt(np.outer(win0, win1))
        fft *= win
        
        # Return filtered image
        return np.real(ifft2(fft))