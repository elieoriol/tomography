# -*- coding: utf-8 -*-
"""
Created on Wed Dec 6 23:48:54 2017

@author: Mathis

"""
import matplotlib.pyplot as plt
from image import Image
import numpy as np
from scipy.fftpack import fft, ifft, fftfreq
from fbp import FBP

class FBP3D():
    "Class to reconstruct images from 3D sinograms"
    def compute(sinogram3d, thetas):
        """
        Compute the reconstruction using 3D FBP for a pencil beam
        
        Given the list of the thetas for which the attenuation was computed to obtain each sinogram, computes a reconstructed 3D image of a reduced_size using Inverse Radon Transform (IRTF).
        
        Parameters
        ----------
        sinogram3d : 3D Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
            
        
        Returns
        -------
        fbps : numpy 3-D array
            The final reconstruction 3D image
        """
        n_z, size = sinogram3d.phantom3d.img.shape[0], sinogram3d.phantom3d.img.shape[1] # number of layers along z, size of each layer
        fbps = np.zeros((n_z, size, size)) # creates an empty array to reconstruct each layer
        for i in range(n_z):
            fbps[i] = FBP.compute(sinogram3d.sngs[i], thetas, size) # every 2-D sinogram is reconstructed an added to the final 3-D image
        return fbps
    
    def plot(sinogram3d, thetas, plane, axis, depth, **options):
        """
        Plot the FBP.
                
        Given the 3D sinogram and the list of the thetas, plots the projection of the reconstructed image on one of the three planes
        at a given depth.
        
        Parameters
        ----------
        sinogram3d : 3D Sinogram instance
        
        thetas : numpy 1-D array
            angles for which the sinogram was computed
            
            
        axis : matplotlib.axes.Axes object

        plane : string
            'xz' to project on the xz plane, 'yz' to project on the yz plane, any other answer will lead to choose xy as the default
            plane

        depth : integer
            index of the layer (of the pre-defined plane) to plot
        
        options : list of options separated by commas
            x_label : sets the x label of the plot
            y_label : sets the y label of the plot
            title : sets the title of the plot
            extent : sets the extent (xmin, xmax, ymin, ymax) of the plot
            aspect : sets the aspect of the plot
            vmin, vmax : to normalize luminance data            
        """
        fbps = FBP3D.compute(sinogram3d, thetas)
        if plane == 'xz':
            fbps = fbps.transpose(1,0,2)
        if plane == 'yz':
            fbps = fbps.transpose(2,1,0)
        if 'x_label' in options:
            axis.set_xlabel(options['x_label'], fontsize=16)
        
        if 'y_label' in options:
            axis.set_ylabel(options['y_label'], fontsize=16)
        
        if 'title' in options:
            axis.set_title(options['title'], fontsize=22)
            
        return axis.imshow(fbps[depth], cmap=plt.cm.Greys_r)    
