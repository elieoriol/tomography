# -*- coding: utf-8 -*-
"""
Created on Wed Dec 6 23:15:31 2017

@author: Mathis

"""

from scipy.misc import imread, imresize
from scipy.ndimage.filters import gaussian_filter
from numpy.random import rand

class Phantom3D_array(object):
    """
    A class that contains a 3D phantom image
    """
    
    def __init__(self, arr, size=-1, blur=-1, noise=-1):
        """
        Constructor
        
        Given an array and options to resize, creates a Phantom object.
        
        Parameters
        ----------
        arr : numpy 3-D array
            array image to create the phantom
        
        size : scalar
            sets the size of the image to resize it (default is -1 : keep image original size)
        
        blur : scalar
            value of sigma to apply a gaussian filter to the phantom (default is -1 : no gaussian blur)
            
        noise : scalar
            gaussian noise maximum amplitude (default is -1 : no gaussian noise)
        """
        
        # Get image as numpy 3-D array
        self.img = arr
        # Resize image
        if size != -1:
            self.img = imresize(self.img, (size, size, size))
        
        size = self.img.shape[0]

