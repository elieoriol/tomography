# -*- coding: utf-8 -*-
"""
Created on Wed Dec 6 23:25:19 2017

@author: Mathis

"""
import numpy as np
from scipy.misc import imrotate
from scipy.interpolate import interp2d, RectBivariateSpline
import sys
from sinogram import Sinogram
from phantom_array import Phantom_array
from phantom_array_3d import Phantom3D_array

class Sinogram3D(object):
    "Class to compute sinograms from 3D phantoms"
    def __init__(self, phantom3d):
        """
        Constructor
        
        Given a 3D phantom object, initializes a 3D sinogram object
        
        Parameters
        ----------
        phantom3d : Phantom3D_array
            object from which the sinogram will be computed
        """
        
        self.phantom3d = phantom3d

    def compute(self, thetas):
        """
        Compute the 3D sinogram for a pencil beam
        
        Given a list of thetas for which attenuation is computed using the Radon Transform, creates an empty list, computes
        the sinogram of every layer (along an arbitrary axis z) of the phantom and adds it to the list.
        
        Parameters
        ----------
        thetas : numpy 1-D array
            angles for which we compute the attenuation
        """
        self.sngs = [] # initializes the list of 2D sinograms
        for i in range(self.phantom3d.img.shape[0]):
            # for every layer, computes the sinogram of the layer, considered as a 2D phantom
            sng = Sinogram(Phantom_array(self.phantom3d.img[i]))
            sng.compute(thetas)
            self.sngs.append(sng)
 

