# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 13:55:45 2017

@author: Elie

Class that contains a phantom image
"""

from scipy.misc import imread, imresize
from scipy.ndimage.filters import gaussian_filter
from numpy.random import rand

class Phantom(object):
    """
    A class that contains a phantom image
    """
    
    def __init__(self, filename, size=-1, blur=-1, noise=-1):
        """
        Constructor
        
        Given a filename and options to resize, add gaussian blur or add gaussian noise, creates a Phantom object.
        
        Parameters
        ----------
        filename : string
            name of the file from which to get the phantom
        
        size : scalar
            sets the size of the image to resize it (default is -1 : keep image original size)
        
        blur : scalar
            value of sigma to apply a gaussian filter to the phantom (default is -1 : no gaussian blur)
            
        noise : scalar
            gaussian noise maximum amplitude (default is -1 : no gaussian noise)
        """
        
        # Get image as numpy 2-D array from filename
        self.img = imread('../../images/' + filename, flatten=True)
        
        # Resize image
        if size != -1:
            self.img = imresize(self.img, (size, size))
        
        size = self.img.shape[0]
        
        # Add gaussian noise : random noise to every pixel of the image, proportional to the image standard deviation
        if noise != -1:
            diff = self.img.std() * rand(size, size) * noise
            diff -= diff.max()/2
            for i in range(size):
                for j in range(size):
                    new_val = self.img[i,j] + int(diff[i, j])
                    if new_val >= 0 and new_val <= 255:
                        self.img[i,j] = new_val
                        
        # Add gaussian blur
        if blur != -1:
            self.img = gaussian_filter(self.img, blur)